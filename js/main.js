var startTime, stopTime;
var current_time = 0;

(function() {
	var schedule = [
		[[720], []],
		[[720], []],
		[[720], []],
		[[720], []],
		[[720], []],
		[[720], []],
		[[720], []]
	];

	var week = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

	var current_mode = -1;

	Array.prototype.append = function(array) {
		this.push.apply(this, array);
	}

	function parseTime(strTime) {
		return (parseInt(strTime[0] + strTime[1]) * 60 + parseInt(strTime[3] + strTime[4])) + ((strTime[6] + strTime[7] == "AM") ? 0 : 12 * 60);
	}

	function fillTwoDigit(num) {
		return ((num < 10) ? "0" : "") + num;
	}

	function timeToString(time) {
		var mid = "AM";
		if (time >= 12 * 60) {
			mid = "PM";
			time -= 12 * 60;
		}
		var hours = Math.floor(time / 60);
		var minutes = time % 60;
		if (hours == 0 && minutes == 0) hours = 12;
		return fillTwoDigit(hours) + ":" + fillTwoDigit(minutes) + " " + mid;
	}

	function getModeEnd(time, weekday, daynight) {
		var end = 2000000000;
		for (var i = 0; i < schedule[weekday][daynight].length; ++i) {
			if (schedule[weekday][daynight][i] < end && schedule[weekday][daynight][i] > time) {
				end = schedule[weekday][daynight][i];
			}
		}
		if (end == 2000000000) end = 0;
		return end;
	}

	function swapFrom(time, weekday) {
		var ar = [[],[]];
		for (var i = 0; i < 2; ++i) {
			var j = 0;
			while (j < schedule[weekday][i].length) {
				if (schedule[weekday][i][j] > time) {
					ar[i].push(schedule[weekday][i][j]);
					schedule[weekday][i].splice(j, 1);
					--j;
				}
				++j;
			}
		}
		schedule[weekday][0].append(ar[1]);
		schedule[weekday][1].append(ar[0]);
	}

	function getCurrentMode(time, weekday) {
		var res = 0, start = -1;
		for (var i = 0; i < 2; ++i){
			for (var j = 0; j < schedule[weekday][0].length; ++j) {
				if (schedule[weekday][i][j] > start && schedule[weekday][i][j] <= time) {
					start = schedule[weekday][i][j];
					res = i;
				}
			}
		}
		if (start == -1) {
			start = 0;
			res = 1;
		}
		var end = getModeEnd(time, weekday, 1 - res);
		var daynight = res;
		if (end == 0) { 
			weekday = (weekday + 1) % 7;
			daynight = 0;
		}
		var next_end = getModeEnd(end, weekday, daynight);
		return [res, start, end, next_end];
	}

	function getLargeImageName(mode) {
		return "images/" + (mode == 0 ? "sun" : "moon") + "_large.png";
	}

	function getTinyImageName(mode) {
		return "images/" + (mode == 0 ? "sun" : "moon") + "_tiny.png";
	}

	function setCurrentTime(time) {
		time = time % (7 * 24 * 60);
		var weekday = Math.floor(time / (24 * 60));
		time = time % (24 * 60);
		$("#cur_time", home).html(timeToString(time) + " " + week[weekday]);
		if($("#mode_slider").val() == "auto") {
			var mode = getCurrentMode(time, weekday);

			if (mode[0] == current_mode) return;
			current_mode = mode[0];

			var daynight = (mode[0] == 0) ? "day" : "night";
			var temperature = $("#temp" + daynight + "_slider").val() + "";
			var home = $("#home");

			$("#cur_temp", home).html(temperature + " &deg;C");
			$("#temp_manual_slider", home).val(temperature);
			$("#temp_manual_slider", home).slider("refresh");
			$("#cur_mode", home).html(timeToString(mode[1]) + " &ndash; " + timeToString(mode[2]));
			$("#cur_mode_img", home).show().attr("src", getLargeImageName(mode[0]));

			$("#next_temp", home).show().html($("#temp" + ((1 - mode[0] == 0) ? "day" : "night") + "_slider").val() + " &deg;C");
			$("#next_mode", home).show().html(timeToString(mode[2]) + " &ndash; " + timeToString(mode[3]));
			$("#next_mode_img", home).show().attr("src", getTinyImageName(1 - mode[0]));
		}
		else {
			var temperature = $("#temp_manual_slider").val() + "";
			var home = $("#home");

			$("#cur_temp", home).html(temperature + " &deg;C");
			$("#cur_mode", home).html("Manual mode");
			$("#cur_mode_img", home).hide();

			$("#next_temp", home).hide();
			$("#next_mode", home).hide();
			$("#next_mode_img", home).hide();
		}
	}

	function generate_temp_span(time, day) {
		//return $("<a href=\"#edit\" data-rel=\"dialog\" data-transition=\"pop\"><img class=\"ui-li-icon\" src=\"images/" + (day ? "sun" : "moon") + "_tiny.png\"/>" + timeToString(time) + "</a>");
		return $("<li><a href=\"#\"><img class=\"ui-li-icon\" src=\"images/" + (day ? "sun" : "moon") + "_tiny.png\"/>" + timeToString(time) + "</a><a href=\"javascript:void(0);\" class=\"btn_del_span\"></a></li>");
	}

	function initScheduleList(index) {
		$("#schedule_list").children().remove("li");
		$.each(schedule[index][0], function(i, e){
			var li = generate_temp_span(e, true);

			var btn = $(".btn_del_span", li);
			btn.on("click", function() {
				schedule[index][0].splice(i, 1);
				li.remove();
				$("#schedule_list").listview("refresh");
			});

			$("#schedule_list").append(li);
		});
		$.each(schedule[index][1], function(i, e){
			var li = generate_temp_span(e, false);

			var btn = $(".btn_del_span", li);
			btn.on("click", function() {
				schedule[index][1].splice(i, 1);
				li.remove();
				$("#schedule_list").listview("refresh");
			});

			$("#schedule_list").append(li);
		});
		$("#schedule_list").listview("refresh");

		$("#add_btn").off();
		$("#add_btn").on("click", function(){
			if ($("#starttime_input").val() == "") return;
			var newtime = parseTime($("#starttime_input").val());
			if (schedule[index][0].indexOf(newtime) != -1 || schedule[index][1].indexOf(newtime) != -1) {
				$("<div>").simpledialog2({
					mode: "blank",
					headerText: "Error",
					headerClose: false,
					blankContent: "<h4>This time span already exists.</h4><a rel='close' data-role='button' href='javascript:void(0);'>Close</a>"
				});
				return;
			}
			var mode = getCurrentMode(newtime, index);

			if (schedule[index][1 - mode[0]].length >= 5) {
				$("<div>").simpledialog2({
					mode: "blank",
					headerText: "Error",
					headerClose: false,
					blankContent: "<h4>You are allowed to add only 5 " + (mode[0] == 0 ? "night" : "day") + " modes.</h4><a rel='close' data-role='button' href='javascript:void(0);'>Close</a>"
				});
				return;
			}

			swapFrom(newtime, index);
			schedule[index][1 - mode[0]].push(newtime);
			$(this).off("click", arguments.callee);
			initScheduleList(index);
		});
	}

	$(document).ready(function(){

		$("#starttime_input").mobiscroll().time({
			theme: "ios",
			display: "bottom",
			mode: "scroller"
		});
		$("#starttime_input").click(function(){
			$(this).mobiscroll("show");
		});
		
		/*$("#endtime_input").mobiscroll().time({
			theme: "ios",
			display: "bottom",
			mode: "scroller"
		});
		$("#endtime_input").click(function(){
			$(this).mobiscroll("show");
		});*/

		$(".temp_plus_btn").click(function(){
			var slider = $("input", $(this).parent().next());
			slider.val(parseFloat(slider.val()) + 0.1);
			slider.change();

		});
		$(".temp_minus_btn").click(function(){
			var slider = $("input", $(this).parent().prev());
			slider.val(parseFloat(slider.val()) - 0.1);
			slider.change();
		});

		$("#weeklist a").each(function(index, element){
			$(element).click(function(){
				$("#modes").bind("pagebeforeshow", function(){
					initScheduleList(index);
					var weekday = $("a[data-weekday=" + index + "]").html();
					document.title = weekday;
					$("h1", this).html(weekday);
					$(this).unbind("pagebeforeshow", arguments.callee);
				});
			});
		});

		var home = $("#home");
		$("#temp_manual_slider", home).on("change", function() {
			var temperature = $("#temp_manual_slider").val() + "";

			$("#cur_temp", home).html(temperature + " &deg;C");

			if ($("#mode_slider").val() == "auto") {
				var time = current_time % (7 * 24 * 60);
				var weekday = Math.floor(time / (24 * 60));
				time = time % (24 * 60);

				var mode = getCurrentMode(time, weekday);
				$("#cur_mode", home).html(timeToString(time) + " &ndash; " + timeToString(mode[2]));
				$("#cur_mode_img", home).hide();

				$("#next_temp", home).show().html($("#temp" + ((1 - mode[0] == 0) ? "day" : "night") + "_slider").val() + " &deg;C");
				$("#next_mode", home).show().html(timeToString(mode[2]) + " &ndash; " + timeToString(mode[3]));
				$("#next_mode_img", home).show().attr("src", getTinyImageName(1 - mode[0]));
			}
		});

		$("#mode_slider", home).on("change", function() {
			if ($(this).val() == "auto") {
				current_mode = -1;
			}
		});

		startTime = function() {
			setCurrentTime(current_time);
			var intervalID = setInterval(function() {
				current_time = (++current_time) % (7 * 24 * 60);
				setCurrentTime(current_time);
			}, 200);
			stopTime = function() {
				/*
		         .-.                     .-.
		      .--' /                     \ '--.
		      '--. \       _______       / .--'
		          \ \   .-"       "-.   / /
		           \ \ /             \ / /
		            \ /               \ /
		             \|   .--. .--.   |/
		              | )/   | |   \( |
		              |/ \__/   \__/ \|
		              /      /^\      \
		              \__    '='    __/
		                |\         /|
		                |\'"VUUUV"'/|
		                \ `"""""""` /
		                 `-._____.-'
		                   / / \ \
		                  / /   \ \
		                 / /     \ \
		              ,-' (       ) `-,
		              `-'._)     (_.'-`

				*/
				clearInterval(intervalID);
			}
		};
		startTime();
	});
})();